
# TODO: добавить настройки (типа число итераций и т.п.)
# TODO: добавить информацию о прогрессе
# TODO: добавить полную ассинхронность
# TODO: добавить хороший хелп
# TODO: добавить список всех опций



import os
import io
import time
import logging
import asyncio

from PIL import Image

from aiogram import Bot, Dispatcher, executor, types
import aiogram.utils.markdown as md
# from aiogram import Bot
# from aiogram.types import BufferedInputFile
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.dispatcher import Dispatcher, FSMContext
from aiogram.dispatcher.filters.state import StatesGroup, State
# from aiogram.types import Message
# from aiogram.utils.executor import start_webhook

from Code import style_transfer as ST
from api_token import API_TOKEN


# Configure logging
logging.basicConfig(level=logging.INFO)
# logging.basicConfig(format=u'%(filename)+13s [ LINE:%(lineno)-4s] %(levelname)-8s [%(asctime)s] %(message)s',
#                     level=logging.DEBUG)


# Initialize bot and dispatcher
loop = asyncio.get_event_loop()
bot = Bot(token=API_TOKEN)
storage = MemoryStorage()
dp = Dispatcher(bot, storage=storage, loop=loop)

bot_name = 'Super Duper Style Transfer Bot'
run_ST_command = 'style_transfer'



class Form(StatesGroup):
    style_img = State()
    content_img = State()


async def download_photo(message):
    await message.photo[-1].download()
    file = await message.photo[-1].get_file()
    return file['file_path']
    # await message.reply(file_path)


async def image_to_byte_array(img: Image):
    byte_arr = io.BytesIO()
    img.save(byte_arr, format='JPEG')  # img.format)
    return byte_arr.getvalue()


async def run_ST(data):
    # style = Image.open(data['style_img_path'])
    # content = Image.open(data['content_img_path'])
    res = ST.full_style_transfer(
        [data['style_img_path'], data['content_img_path']],
        dict(
            opt_img_rand=False
        ),
        dict(
            style_loss_weight_callback=ST.gatys_style_loss_weight_callback
        ),
        dict(
            equalize_content_and_style=False,
            wanted_style_losses_contrib=False,
            tv_loss_weight=100.0,
            max_iter=500,
            print_iter=False,
            show_iter=False,
            history_update_freq=False,
            verbose=0
        )
    )
    return await image_to_byte_array(res.opt_img)


async def run_ST2(data):
    qqq = 0
    for i in range(10 ** 10):
        qqq = i
    return None


@dp.message_handler(commands=['start', 'help'])
async def send_welcome(message: types.Message):
    """Команды `/start` и `/help`."""
    await message.reply(
        f'Привет!\n'
        f'Меня зовут **{bot_name}**.\n'
        f'Я умею стилизовать изображения. Для этого введи команду /{run_ST_command}.'
    )


@dp.message_handler(commands=run_ST_command)
async def welcome(message: types.Message):
    await message.answer('Пришлите сначала стилевое изображение')  # , а затем контентное изображение')
    await Form.style_img.set()


@dp.message_handler(state=Form.style_img, content_types=['photo'])
async def process_style_img(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['style_img_path'] = await download_photo(message)

        await Form.next()
        await message.answer(f'Теперь пришлите контентное изображение')  # . Состояние: {data}


@dp.message_handler(state=Form.style_img)
async def cancel_style_img(message: types.Message, state: FSMContext):
    await message.answer(f'Не получил от Вас стилевое изображение, поэтому процесс переноса стиля прерван')
    await state.finish()


@dp.message_handler(state=Form.content_img, content_types=['photo'])
async def process_content_img(message: types.Message, state: FSMContext):
    async with state.proxy() as data:
        data['content_img_path'] = await download_photo(message)

        await message.answer(f'Скоро я пришлю Вам стилизацию')  # . Состояние: {data}

        start_time = time.time()
        task = loop.create_task(run_ST2(data))
        # TODO: писать, сколько времени осталось
        # task = asyncio.create_task(run_ST(data))
        result = await task
        # result = await run_ST(data)
        end_time = time.time()

        if result is None:
            return

        markup = types.ReplyKeyboardRemove()

        # md.text('Hi! Nice to meet you,', md.bold(data['name'])),
        # md.text('Age:', md.code(data['age'])),
        # md.text('Gender:', data['gender']),
        # sep='\n',

        await message.reply_photo(
            result, caption=md.text('Стилизация заняла', md.bold(f'{end_time - start_time:.0f} секунд')),
            reply_markup=markup, parse_mode=types.ParseMode.MARKDOWN
        )

        await state.finish()


@dp.message_handler(state=Form.style_img)
async def cancel_content_img(message: types.Message, state: FSMContext):
    await message.answer(f'Не получил от Вас контентное изображение, поэтому процесс переноса стиля прерван')
    await state.finish()


@dp.message_handler()
async def echo(message: types.Message):
    await message.answer(
        f'Для запуска процесса переноса стиля введи команду /{run_ST_command}'
    )


#
#
# # @dp.message_handler(regexp='(^cat[s]?$|puss)')
# # async def cats(message: types.Message):
# #     with open('images/content/dog.jpg', 'rb') as photo:
# #         await message.reply_photo(photo, caption='Cats are here 😺')
#
#
# @dp.message_handler()
# async def echo(message: types.Message):
#     await message.answer(message.text)
#
#
# @dp.message_handler(content_types=['photo'])
# async def handle_docs_photo(message):
#     # print(message.photo)
#     # print(message.photo[-1].__dict__)
#     # print(tmp)
#
#     # file_path = await message.photo[-1].get_file()
#     #
#     # # tmp = io.BytesIO()
#     # res: io.BytesIO = await bot.download_file(file_path)
#     #
#     # tmp = res.getbuffer()
#     # print(tmp)
#     # with open("output.png", "wb") as f:
#     #     f.write(tmp)
#
#     # with open('tmp.jpg', 'wb') as fout:
#     #     fout.write(res)
#
#     # print(res)
#     # res.seek(0)
#     # Image.frombytes()
#     # img = await Image.open(res)  #, 'wb+')
#     # print(img)
#
#
#     await message.photo[-1].download()
#     file = await message.photo[-1].get_file()
#     file_path = file['file_path']
#     await message.reply(file_path)
#
#
#     # print(type(res))
#     #
#     # # res.read()
#     # # res.seek(0)
#     #
#     # img = await Image.open(res)  #, 'wb+')
#     # print(img)
#
#
#
#     # print(res.__dict__)
#     # tmp = res.getvalue()
#     # print(tmp)
#
#     # # Move cursor back to the beginning of the buffer
#     # res.seek(0)
#     #
#     # # Read all data from the buffer
#     # stream_data = res.read()
#     #
#     # # The stream_data is type 'bytes', immutable
#     # print(type(stream_data))
#     # print(stream_data)
#
#     # await message.reply('asdasd')
#
#     # await message.photo[-1].download('test.jpg')
#     # # print(photo)
#     # with open('test.jpg', 'rb') as photo:
#     #     # pass
#     #     await message.reply_photo(photo, caption='Вот ваша фотка')


if __name__ == '__main__':
    executor.start_polling(dp, loop=loop, skip_updates=True)




# TOKEN = os.environ['TOKEN']


# WEBHOOK_HOST = 'https://типа url'  # name your app
# WEBHOOK_PATH = '/webhook/'
# WEBHOOK_URL = f"{WEBHOOK_HOST}{WEBHOOK_PATH}"

# WEBAPP_HOST = '0.0.0.0'
# WEBAPP_PORT = os.environ.get('PORT')

# logging.basicConfig(level=logging.INFO)

# bot = Bot(token=TOKEN)
# dp = Dispatcher(bot, storage=storage)


# storage = MemoryStorage()
# class Form(StatesGroup):
#     hey = State()
#     hey2 = State()
#     hey3 = State()


# async def on_startup(dp):
#     await bot.set_webhook(WEBHOOK_URL)
#     logging.info(dp)


# async def on_shutdown(dp):
#     logging.info(dp)


# if __name__ == '__main__':
#     start_webhook(dispatcher=dp, webhook_path=WEBHOOK_PATH,
#                   on_startup=on_startup, on_shutdown=on_shutdown,
#                   host=WEBAPP_HOST, port=WEBAPP_PORT)




